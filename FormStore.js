import create from 'zustand';


export const useFormStore = create(set => ({
    formValues: [],
    addValues: (value) => set(state => ({ formValues: [...state.formValues, value] })),
    clearValues: () => set({ formValues: []}),
    increasePopulation: () => set(state => ({ formValues: [...state.formValues + 1 ]})),

}))

export const useStore = create(set => ({
    bears: 0,
    increasePopulation: () => set(state => ({ bears: state.bears + 1 })),
    removeAllBears: () => set({ bears: 0 })
  }))