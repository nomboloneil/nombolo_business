import {useState, useEffect} from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native';
import * as Location from 'expo-location';

export default useLocationSuggester = () => {
    const [location, setLocation] = useState(null);
    const [geo, setGeo] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    useEffect(() => {
        (async () => {
          let { status } = await Location.requestForegroundPermissionsAsync();
          if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
            return;
          }
          Location.setGoogleApiKey("AIzaSyDApoxd20oQKzx3PzWr_sACWsQ0HRGGLN0")
          let location = await Location.getCurrentPositionAsync({});
          setLocation(location);
        })();
      }, []);
   
    useEffect(() => {
        (async () => {
          if (location !== null) {
            let reverseGeo = await Location.reverseGeocodeAsync(location.coords, {
                useGoogleMaps: true,
            });
            setGeo(reverseGeo);
          }
        })();
      }, [location]);

      return geo
}