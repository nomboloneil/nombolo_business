import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, TextInput, Dimensions, ScrollView } from 'react-native'
import { platformText } from './globalStyles'
// export default function About() {
//     return (
//         <View>
//         </View>
//     )
// }

export default  About = () => {
    const { height, width } = Dimensions.get('window')
    const [description, setDescription] = useState(null)
    const [contentHeight, setContentHeight] = useState(40)
    useEffect(() => {
        console.log(contentHeight)
    }, [contentHeight])
    return (
      <>
  <ScrollView contentContainerStyle={{marginTop: 20, marginBottom: 200}}>
           <View><Text style={styles.aboutTextStyle} >About</Text></View> 
      <View style={{marginBottom: 400}}>
      { description ? <Text style={styles.descriptionText} >Business Description</Text> : null}
         <TextInput 
            style={{...styles.input, width: width * .9, height: contentHeight}}
            placeholder={'Business Description'}
            multiline={true}
            textAlignVertical={'top'}
            onChangeText={setDescription}
            maxLength={1000}
            onContentSizeChange={(event) => setContentHeight(event.nativeEvent.contentSize.height + 23)}
            />
      { description ? <Text style={styles.descriptionText} > {description.length}/ 1000 characters</Text> : null}

      </View>
      </ScrollView>
      </>
    )
  }

  const styles = StyleSheet.create({
    input: {
        borderColor: "lightgrey",
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center', 
      },
      aboutTextStyle: {
        paddingBottom: 20,
        fontWeight: "bold", 
        fontSize: 15,
        ...platformText
      },
      descriptionText: {
        ...platformText, 
        color: "lightgrey",
      }
  })