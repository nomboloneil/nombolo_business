import React, {useEffect, useState, useRef} from 'react';
import { Animated, Text, View, StyleSheet, TextInput, Button, Alert,ScrollView, useWindowDimensions, KeyboardAvoidingView, Platform } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import Constants from 'expo-constants';
import platformText from './globalStyles'
import useLocationSuggester from './useLocationSuggester'
import AvoidKeyboard from './AvoidKeyboard'
import { useKeyboard } from '@react-native-community/hooks'
// import DeviceInfo from 'react-native-device-info';
import {useFormStore, useStore} from './FormStore'

export default () => {
 //const geo = useLocationSuggester()
 const keyboard = useKeyboard()
  const [keyPadding, setKeyPadding] = useState(50)
  const windowWidth = useWindowDimensions().width;
  const windowHeight = useWindowDimensions().height;
  const { register, setValue, handleSubmit, control, reset, errors, formState } = useForm();
  const fadeAnim = useRef(new Animated.Value(0)).current;
 const values = useFormStore(state => state.formValues)
  const setValues = useFormStore(state => state.addValues)
  const {formValues, addValues} = useFormStore()
  const {bears, increasePopulation} = useStore()

  // useEffect(() => {
  //   if(formState) {
  //     console.log('form State',formState)
  //   }
    
  // }, [formState])
  useEffect(() => {
    console.log('bers',bears)
    
  }, [bears])
  useEffect(() =>{
    console.log('values', values)
   //dddddddd console.log('setValues', addValues)

  })
  useEffect(() => {
    register('phone', 1)
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: true,
    }).start();
    
    // DeviceInfo.getPhoneNumber().then((phoneNumber) => {
    //     if (phoneNumber) {
    //       // setValue({"phone": phoneNumber})

    //     }
    //     // Android: null return: no permission, empty string: unprogrammed or empty SIM1, e.g. "+15555215558": normal return value
    // });
    
    if(!keyboard.keyboardShown) {
      setKeyPadding(0)
    }
  },[keyboard])

  // useEffect(() => {
  //   if (keyPadding) {
  //     console.log("keypadding, ", keyPadding)
  //   }
    
  // }, [keyPadding])
  const increment = () => {
    setKeyPadding(prevValue=> prevValue +50)
  }
  const onSubmit = data => {
    //console.log(data);
    addValues(data)
    //increasePopulation()
    
  };



  const onChange = arg => {
    return {
      value: arg.nativeEvent.text,
    };
  };
  errors && console.error('info input errors', errors);

  // useEffect(() => {
  //     if (geo) {
  //       console.log('got in here')

  //         console.log('geo', geo)
          
  //           reset({
  //             city: geo[1].city ?? 'city',
  //             address: geo[1].name,
  //             zip: geo[1].postalCode ?? "zip",
  //             state: geo[1].region ?? "state"
  //           })
          
  //     }
  // }, [geo])

  return (

    <Animated.View style={[
      styles.container,
      {
        // Bind opacity to animated value
        opacity: fadeAnim
      }
    ]}>
   
      <Text style={styles.label}></Text>
      <Controller
        control={control}
        render={({field: { onChange, onBlur, value }}) => (
          <TextInput
            placeholder={"Business Name"}
            style={styles.input}
            onBlur={onBlur}
            onChangeText={value => onChange(value)}
            value={value}
            onEndEditing={value => console.log(value)}
            onFocus={() => setKeyPadding(150)}
          />
        )}
        name="name"
        rules={{ required: true }}
      />
      <Text style={styles.label}>Address and Contact Info</Text>
      <Controller
        control={control}
        render={({field: { onChange, onBlur, value }}) => (
          <TextInput
            placeholder={"Street Address"}
            style={{...styles.input, width: windowWidth * .9}}
            onBlur={onBlur}
            onChangeText={value => onChange(value)}
            value={value}
            onFocus={() => setKeyPadding(150)}

          />
        )}
        name="address"
        rules={{ required: true }}
      />
     
<View style={{flexDirection: "row"}}>
    <View style={{flex: 1}}>
        <Controller
            control={control}
            render={({field: { onChange, onBlur, value }}) => (
            <TextInput
                placeholder={"City"}
                style={{...styles.input, width: windowWidth * .6}}
                onBlur={onBlur}
                onChangeText={value => addValues(value)}
                value={formValues}
                onFocus={increment}

            />
            )}
            name="city"
            rules={{ required: true }}
        />
    </View>
    <View style={{flex: 1}}>
        <Controller
            control={control}
            render={({field: { onChange, onBlur, value }}) => (
            <TextInput
                placeholder={"State"}
                style={{...styles.input, width: windowWidth * .6}}
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                onFocus={increment}

            />
            )}
            name="state"
            rules={{ required: true }}
        />
    </View>
</View>     
<View style={{flexDirection: "row"}}>
    <View style={{flex: 1}}>
        <Controller
            control={control}
            render={({field: { onChange, onBlur, value }}) => (
            <TextInput
              {...register("phone")}
                textContentType={"telephoneNumber"}
                keyboardType={"phone-pad"}
                placeholder={"Phone"}
                style={{...styles.input, width: windowWidth * .9}}
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                onFocus={increment}

            />
            )}
            name="phone"
            rules={{ required: true }}
        />
    </View>
    <View style={{flex: 1}}>
        <Controller
            control={control}
            render={({field: { onChange, onBlur, value }}) => (
            <TextInput
                placeholder={"Zipcode"}
                style={{...styles.input, width: windowWidth * .9}}
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                onFocus={increment}

            />
            )}
            name="zip"
            rules={{ required: true }}
        />
    </View>
</View>
<Controller
        control={control}
        render={({field: { onChange, onBlur, value }}) => (
          <TextInput
            placeholder={"Website"}
            style={{...styles.input, width: windowWidth * .9}}
            onBlur={onBlur}
            onChangeText={value => onChange(value)}
            value={value}
            onFocus={increment}

          />
        )}
        name="website"
        rules={{ required: true }}
      />
      

      <View style={styles.button}>
        <Button
          title="Reset"
          onPress={() => {
            reset({
              name: 'Business Name',
              address: 'Street Address'
            })
          }}
        />
      </View>

      <View style={styles.button}>
        <Button          
          title="Button"
          onPress={handleSubmit(onSubmit)}
        />
      </View>
    </Animated.View>

  );
};

const styles = StyleSheet.create({
  label: {
    fontFamily: 'Helvetica',
    color: 'black',
    fontWeight: '600',
    margin: 20,
    marginLeft: 0,
  },
  button: {
    marginTop: 40,
    color: 'white',
    height: 40,
    backgroundColor: '#ec5990',
    borderRadius: 4,
  },
  container: {
    position: 'absolute',
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    padding: 10

  },
  input: {
    height: 40,
    borderColor: "lightgrey",
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center', 
  },
});