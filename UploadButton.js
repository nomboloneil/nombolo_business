import React from 'react'
import { View, Text } from 'react-native'
import { EvilIcons, Ionicons } from '@expo/vector-icons';
import { platformText } from './globalStyles'
export default function UploadButton() {
    return (
        <View>
        <View style={uploadText}>
        <EvilIcons name="image" size={50} color="gray"></EvilIcons>
  
          <Text style={{color: 'gray'}}>Upload Image</Text>   
      
        </View>
  
      </View>
    )
}

const uploadContainer = {
    backgroundColor: "transparent",
    borderColor: "gray",
    borderRadius: 5,
    height: 550,
    width: 350,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignContent: "center",
  }
  const uploadText = {
    fontSize: 13,
    color: "gray",
    ...platformText,
    paddingVertical: 8,
    borderWidth: 2,
    borderColor: "gray",
    borderRadius: 10,
    backgroundColor: "transparent",
    color: "#20232a",
    display: "flex",
    alignItems: "center",
  }
