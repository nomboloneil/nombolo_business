import React, { Component } from 'react';
import { View, Text, Dimensions, Platform, TouchableOpacity, TextInput, StyleSheet, SafeAreaView, KeyboardAvoidingView } from 'react-native';

import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
const { width, height } = Dimensions.get('window');
import InfoInputs from './InfoInputs'
import platformText from './globalStyles'
import Welcome from './Welcome'
import Hours from './Hours'
import About from './About'
import UploadButton from './UploadButton'

class ExampleTwo extends Component {

  render() {
    return (
      <View style={{ flex: 1, marginTop: 50 }}>
      
        <Header/>
        <ProgressSteps 
          {...progressStepsStyle}>
          <ProgressStep
            label="Info"
            onNext={onNextStep}
            onPrevious={onPrevStep}
            scrollViewProps={defaultScrollViewProps}
            nextBtnStyle={buttonStyle}
            previousBtnText={'go back'}
            nextBtnTextStyle={buttonTextStyle}
            previousBtnTextStyle={backButtonTextStyle}
            previousBtnStyle={backButtonStyle}
          >
          
          
              <Info/>
          </ProgressStep>
          <ProgressStep
            label="Hours"
            onPrevious={onPrevStep}
            scrollViewProps={defaultScrollViewProps}
            nextBtnStyle={buttonStyle}
            nextBtnTextStyle={buttonTextStyle}
            previousBtnTextStyle={buttonTextStyle}
            previousBtnStyle={buttonStyle}
          >
            <View style={{ alignItems: 'center' }}>
              <Hours/>
            </View>
          </ProgressStep>
          <ProgressStep
            label="About"
            onPrevious={onPrevStep}
            scrollViewProps={defaultScrollViewProps}
            nextBtnStyle={buttonStyle}
            nextBtnTextStyle={buttonTextStyle}
            previousBtnTextStyle={buttonTextStyle}
            previousBtnStyle={buttonStyle}
          >
            <View style={{ alignItems: 'center' }}>
              <About/>
            </View>
          </ProgressStep>
      
          <ProgressStep
            label="Review"
            onPrevious={onPrevStep}
            onSubmit={onSubmitSteps}
            scrollViewProps={defaultScrollViewProps}
            nextBtnStyle={buttonStyle}
            nextBtnTextStyle={buttonTextStyle}
            previousBtnTextStyle={buttonTextStyle}
            previousBtnStyle={buttonStyle}
          >
            <View style={{ alignItems: 'center', paddingTop: 150 }}>
              <Welcome/>
            </View>
          </ProgressStep>
        </ProgressSteps>
      </View>
    );
  }
}

const Header = () => {
  return <Text style={headerStyle}>Create Profile</Text>
}

const BlueButton = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} style={{ width: "100%" }}>
            <View style={styles.blueButton}>
                <Text style={styles.buttonText}>{props.text}</Text>
            </View>
        </TouchableOpacity>
    );
};

const Info = () => {
  return (
    <View style={{paddingBottom: 500}}>
      <UploadButton/>
      <InfoInputs/>
    
    </View>
  )
}


const errorStyles = (error) => {
  if (error) {    
     return { ...styles.signUpInput, borderColor: 'red' } 
  } else {
      return [styles.signUpInput]
  }
}


// const BlueButtonWithIcon = (props) => {
//   return (
//       <TouchableOpacity onPress={props.onPress} style={styles.buttonContainer}>
//           <View style={styles.blueButtonView}>
  //               <EvilIcons
  //                   name={props.icon}
  //                   size={props.size}
  //                   color={props.color}
  //                   style={props.style}
  //               />
//               <Text style={styles.buttonText}>
//                   {props.text}
//               </Text>
//           </View>
//       </TouchableOpacity>
//   );
// };
const input = {
    height:40,
    width: 50,
    margin: 12,
    borderWidth: 1,
}


const headerStyle = {
  fontSize: 24,
  textAlign: "center",
  ...platformText
}
const aboutTextStyle = {
  fontWeight: "bold", 
  fontSize: 14,
  ...platformText
}

const progressStepsStyle = {
  activeStepIconBorderColor: '#5BA9AB',
  activeLabelColor: '#5BA9AB',
  activeStepNumColor: 'white',
  activeStepIconColor: '#5BA9AB',
  completedStepIconColor: '#5BA9AB',
  completedProgressBarColor: '#5BA9AB',
  completedCheckColor: 'white',
  topOffset: 0,
  marginBottom: 0
};

const buttonTextStyle = {
  textAlign: 'center',
  color: 'white',
  fontWeight: 'bold',
};
const backButtonTextStyle = {
  textAlign: 'center',
  color: 'black',
  fontWeight: 'bold',
};
const backButtonStyle = {
  backgroundColor: "grey",
  borderColor: "black",
  borderRadius: 20,
  width: width/4,
}
const buttonStyle = {
  backgroundColor: "#5BA9AB",
  borderRadius: 20,
  width: width/4,
}
const defaultScrollViewProps = {
  keyboardShouldPersistTaps: 'handled',
  contentContainerStyle: {
    flex: 1,
    justifyContent: 'center'
  }
};

const styles = StyleSheet.create({
  signUpInput: {
    height: 40,
    borderColor: "lightgrey",
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center', 
  }
})

const onNextStep = () => {
  console.log('called next step');
};

const onPrevStep = () => {
  console.log('called previous step');
};

const onSubmitSteps = () => {
  console.log('called on submit step.');
};

export default ExampleTwo;
