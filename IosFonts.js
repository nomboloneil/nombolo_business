import React, { Component } from 'react';
import { ScrollView, Text, StyleSheet} from 'react-native';

styles=StyleSheet.create({
    scroller: {
        flex: 1,
    }
});

export default class IosFonts extends Component{
  render (){
    return(
      <ScrollView style={styles.scroller}>
        <Text style={{fontFamily: 'Academy Engraved LET'}}>Academy Engraved LET </Text>
        <Text style={{fontFamily: 'AcademyEngravedLetPlain'}}>AcademyEngravedLetPlain </Text>
        <Text style={{fontFamily: 'Al Nile'}}>Al Nile </Text>
        <Text style={{fontFamily: 'AlNile-Bold'}}>AlNile-Bold </Text>
        <Text style={{fontFamily: 'American Typewriter'}}>American Typewriter </Text>
        <Text style={{fontFamily: 'AmericanTypewriter-Bold'}}>AmericanTypewriter-Bold </Text>
        <Text style={{fontFamily: 'AmericanTypewriter-Condensed'}}>AmericanTypewriter-Condensed </Text>
        <Text style={{fontFamily: 'AmericanTypewriter-CondensedBold'}}>AmericanTypewriter-CondensedBold </Text>
        <Text style={{fontFamily: 'AmericanTypewriter-CondensedLight'}}>AmericanTypewriter-CondensedLight </Text>
        <Text style={{fontFamily: 'AmericanTypewriter-Light'}}>AmericanTypewriter-Light </Text>
        <Text style={{fontFamily: 'Apple Color Emoji'}}>Apple Color Emoji </Text>
        <Text style={{fontFamily: 'Apple SD Gothic Neo'}}>Apple SD Gothic Neo </Text>
        <Text style={{fontFamily: 'AppleColorEmoji'}}>AppleColorEmoji </Text>
        <Text style={{fontFamily: 'AppleSDGothicNeo-Bold'}}>AppleSDGothicNeo-Bold </Text>
        <Text style={{fontFamily: 'AppleSDGothicNeo-Light'}}>AppleSDGothicNeo-Light </Text>
        <Text style={{fontFamily: 'AppleSDGothicNeo-Medium'}}>AppleSDGothicNeo-Medium </Text>
        <Text style={{fontFamily: 'AppleSDGothicNeo-Regular'}}>AppleSDGothicNeo-Regular </Text>
        <Text style={{fontFamily: 'AppleSDGothicNeo-SemiBold'}}>AppleSDGothicNeo-SemiBold </Text>
        <Text style={{fontFamily: 'AppleSDGothicNeo-Thin'}}>AppleSDGothicNeo-Thin </Text>
        <Text style={{fontFamily: 'AppleSDGothicNeo-UltraLight'}}>AppleSDGothicNeo-UltraLight </Text>
        <Text style={{fontFamily: 'Arial'}}>Arial </Text>
        <Text style={{fontFamily: 'Arial Hebrew'}}>Arial Hebrew </Text>
        <Text style={{fontFamily: 'Arial Rounded MT Bold'}}>Arial Rounded MT Bold </Text>
        <Text style={{fontFamily: 'Arial-BoldItalicMT'}}>Arial-BoldItalicMT </Text>
        <Text style={{fontFamily: 'Arial-BoldMT'}}>Arial-BoldMT </Text>
        <Text style={{fontFamily: 'Arial-ItalicMT'}}>Arial-ItalicMT </Text>
        <Text style={{fontFamily: 'ArialHebrew'}}>ArialHebrew </Text>
        <Text style={{fontFamily: 'ArialHebrew-Bold'}}>ArialHebrew-Bold </Text>
        <Text style={{fontFamily: 'ArialHebrew-Light'}}>ArialHebrew-Light </Text>
        <Text style={{fontFamily: 'ArialMT'}}>ArialMT </Text>
        <Text style={{fontFamily: 'ArialRoundedMTBold'}}>ArialRoundedMTBold </Text>
        <Text style={{fontFamily: 'Avenir'}}>Avenir </Text>
        <Text style={{fontFamily: 'Avenir Next'}}>Avenir Next </Text>
        <Text style={{fontFamily: 'Avenir Next Condensed'}}>Avenir Next Condensed </Text>
        <Text style={{fontFamily: 'Avenir-Black'}}>Avenir-Black </Text>
        <Text style={{fontFamily: 'Avenir-BlackOblique'}}>Avenir-BlackOblique </Text>
        <Text style={{fontFamily: 'Avenir-Book'}}>Avenir-Book </Text>
        <Text style={{fontFamily: 'Avenir-BookOblique'}}>Avenir-BookOblique </Text>
        <Text style={{fontFamily: 'Avenir-Heavy'}}>Avenir-Heavy </Text>
        <Text style={{fontFamily: 'Avenir-HeavyOblique'}}>Avenir-HeavyOblique </Text>
        <Text style={{fontFamily: 'Avenir-Light'}}>Avenir-Light </Text>
        <Text style={{fontFamily: 'Avenir-LightOblique'}}>Avenir-LightOblique </Text>
        <Text style={{fontFamily: 'Avenir-Medium'}}>Avenir-Medium </Text>
        <Text style={{fontFamily: 'Avenir-MediumOblique'}}>Avenir-MediumOblique </Text>
        <Text style={{fontFamily: 'Avenir-Oblique'}}>Avenir-Oblique </Text>
        <Text style={{fontFamily: 'Avenir-Roman'}}>Avenir-Roman </Text>
        <Text style={{fontFamily: 'AvenirNext-Bold'}}>AvenirNext-Bold </Text>
        <Text style={{fontFamily: 'AvenirNext-BoldItalic'}}>AvenirNext-BoldItalic </Text>
        <Text style={{fontFamily: 'AvenirNext-DemiBold'}}>AvenirNext-DemiBold </Text>
        <Text style={{fontFamily: 'AvenirNext-DemiBoldItalic'}}>AvenirNext-DemiBoldItalic </Text>
        <Text style={{fontFamily: 'AvenirNext-Heavy'}}>AvenirNext-Heavy </Text>
        <Text style={{fontFamily: 'AvenirNext-HeavyItalic'}}>AvenirNext-HeavyItalic </Text>
        <Text style={{fontFamily: 'AvenirNext-Italic'}}>AvenirNext-Italic </Text>
        <Text style={{fontFamily: 'AvenirNext-Medium'}}>AvenirNext-Medium </Text>
        <Text style={{fontFamily: 'AvenirNext-MediumItalic'}}>AvenirNext-MediumItalic </Text>
        <Text style={{fontFamily: 'AvenirNext-Regular'}}>AvenirNext-Regular </Text>
        <Text style={{fontFamily: 'AvenirNext-UltraLight'}}>AvenirNext-UltraLight </Text>
        <Text style={{fontFamily: 'AvenirNext-UltraLightItalic'}}>AvenirNext-UltraLightItalic </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-Bold'}}>AvenirNextCondensed-Bold </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-BoldItalic'}}>AvenirNextCondensed-BoldItalic </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-DemiBold'}}>AvenirNextCondensed-DemiBold </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-DemiBoldItalic'}}>AvenirNextCondensed-DemiBoldItalic </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-Heavy'}}>AvenirNextCondensed-Heavy </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-HeavyItalic'}}>AvenirNextCondensed-HeavyItalic </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-Italic'}}>AvenirNextCondensed-Italic </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-Medium'}}>AvenirNextCondensed-Medium </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-MediumItalic'}}>AvenirNextCondensed-MediumItalic </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-Regular'}}>AvenirNextCondensed-Regular </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-UltraLight'}}>AvenirNextCondensed-UltraLight </Text>
        <Text style={{fontFamily: 'AvenirNextCondensed-UltraLightItalic'}}>AvenirNextCondensed-UltraLightItalic </Text>
        <Text style={{fontFamily: 'Bangla Sangam MN'}}>Bangla Sangam MN </Text>
        <Text style={{fontFamily: 'Baskerville'}}>Baskerville </Text>
        <Text style={{fontFamily: 'Baskerville-Bold'}}>Baskerville-Bold </Text>
        <Text style={{fontFamily: 'Baskerville-BoldItalic'}}>Baskerville-BoldItalic </Text>
        <Text style={{fontFamily: 'Baskerville-Italic'}}>Baskerville-Italic </Text>
        <Text style={{fontFamily: 'Baskerville-SemiBold'}}>Baskerville-SemiBold </Text>
        <Text style={{fontFamily: 'Baskerville-SemiBoldItalic'}}>Baskerville-SemiBoldItalic </Text>
        <Text style={{fontFamily: 'Bodoni 72'}}>Bodoni 72 </Text>
        <Text style={{fontFamily: 'Bodoni 72 Oldstyle'}}>Bodoni 72 Oldstyle </Text>
        <Text style={{fontFamily: 'Bodoni 72 Smallcaps'}}>Bodoni 72 Smallcaps </Text>
        <Text style={{fontFamily: 'Bodoni Ornaments'}}>Bodoni Ornaments </Text>
        <Text style={{fontFamily: 'BodoniOrnamentsITCTT'}}>BodoniOrnamentsITCTT </Text>
        <Text style={{fontFamily: 'BodoniSvtyTwoITCTT-Bold'}}>BodoniSvtyTwoITCTT-Bold </Text>
        <Text style={{fontFamily: 'BodoniSvtyTwoITCTT-Book'}}>BodoniSvtyTwoITCTT-Book </Text>
        <Text style={{fontFamily: 'BodoniSvtyTwoITCTT-BookIta'}}>BodoniSvtyTwoITCTT-BookIta </Text>
        <Text style={{fontFamily: 'BodoniSvtyTwoOSITCTT-Bold'}}>BodoniSvtyTwoOSITCTT-Bold </Text>
        <Text style={{fontFamily: 'BodoniSvtyTwoOSITCTT-Book'}}>BodoniSvtyTwoOSITCTT-Book </Text>
        <Text style={{fontFamily: 'BodoniSvtyTwoSCITCTT-Book'}}>BodoniSvtyTwoSCITCTT-Book </Text>
        <Text style={{fontFamily: 'Bradley Hand'}}>Bradley Hand </Text>
        <Text style={{fontFamily: 'BradleyHandITCTT-Bold'}}>BradleyHandITCTT-Bold </Text>
        <Text style={{fontFamily: 'Chalkboard SE'}}>Chalkboard SE </Text>
        <Text style={{fontFamily: 'ChalkboardSE-Bold'}}>ChalkboardSE-Bold </Text>
        <Text style={{fontFamily: 'ChalkboardSE-Light'}}>ChalkboardSE-Light </Text>
        <Text style={{fontFamily: 'ChalkboardSE-Regular'}}>ChalkboardSE-Regular </Text>
        <Text style={{fontFamily: 'Chalkduster'}}>Chalkduster </Text>
        <Text style={{fontFamily: 'Chalkduster'}}>Chalkduster </Text>
        <Text style={{fontFamily: 'Cochin'}}>Cochin </Text>
        <Text style={{fontFamily: 'Cochin-Bold'}}>Cochin-Bold </Text>
        <Text style={{fontFamily: 'Cochin-BoldItalic'}}>Cochin-BoldItalic </Text>
        <Text style={{fontFamily: 'Cochin-Italic'}}>Cochin-Italic </Text>
        <Text style={{fontFamily: 'Copperplate'}}>Copperplate </Text>
        <Text style={{fontFamily: 'Copperplate-Bold'}}>Copperplate-Bold </Text>
        <Text style={{fontFamily: 'Copperplate-Light'}}>Copperplate-Light </Text>
        <Text style={{fontFamily: 'Courier'}}>Courier </Text>
        <Text style={{fontFamily: 'Courier New'}}>Courier New </Text>
        <Text style={{fontFamily: 'Courier-Bold'}}>Courier-Bold </Text>
        <Text style={{fontFamily: 'Courier-BoldOblique'}}>Courier-BoldOblique </Text>
        <Text style={{fontFamily: 'Courier-Oblique'}}>Courier-Oblique </Text>
        <Text style={{fontFamily: 'CourierNewPS-BoldItalicMT'}}>CourierNewPS-BoldItalicMT </Text>
        <Text style={{fontFamily: 'CourierNewPS-BoldMT'}}>CourierNewPS-BoldMT </Text>
        <Text style={{fontFamily: 'CourierNewPS-ItalicMT'}}>CourierNewPS-ItalicMT </Text>
        <Text style={{fontFamily: 'CourierNewPSMT'}}>CourierNewPSMT </Text>
        <Text style={{fontFamily: 'Damascus'}}>Damascus </Text>
        <Text style={{fontFamily: 'DamascusBold'}}>DamascusBold </Text>
        <Text style={{fontFamily: 'DamascusLight'}}>DamascusLight </Text>
        <Text style={{fontFamily: 'DamascusMedium'}}>DamascusMedium </Text>
        <Text style={{fontFamily: 'DamascusSemiBold'}}>DamascusSemiBold </Text>
        <Text style={{fontFamily: 'Devanagari Sangam MN'}}>Devanagari Sangam MN </Text>
        <Text style={{fontFamily: 'DevanagariSangamMN'}}>DevanagariSangamMN </Text>
        <Text style={{fontFamily: 'DevanagariSangamMN-Bold'}}>DevanagariSangamMN-Bold </Text>
        <Text style={{fontFamily: 'Didot'}}>Didot </Text>
        <Text style={{fontFamily: 'Didot-Bold'}}>Didot-Bold </Text>
        <Text style={{fontFamily: 'Didot-Italic'}}>Didot-Italic </Text>
        <Text style={{fontFamily: 'DiwanMishafi'}}>DiwanMishafi </Text>
        <Text style={{fontFamily: 'Euphemia UCAS'}}>Euphemia UCAS </Text>
        <Text style={{fontFamily: 'EuphemiaUCAS-Bold'}}>EuphemiaUCAS-Bold </Text>
        <Text style={{fontFamily: 'EuphemiaUCAS-Italic'}}>EuphemiaUCAS-Italic </Text>
        <Text style={{fontFamily: 'Farah'}}>Farah </Text>
        <Text style={{fontFamily: 'Futura'}}>Futura </Text>
        <Text style={{fontFamily: 'Futura-CondensedExtraBold'}}>Futura-CondensedExtraBold </Text>
        <Text style={{fontFamily: 'Futura-CondensedMedium'}}>Futura-CondensedMedium </Text>
        <Text style={{fontFamily: 'Futura-Medium'}}>Futura-Medium </Text>
        <Text style={{fontFamily: 'Futura-MediumItalic'}}>Futura-MediumItalic </Text>
        <Text style={{fontFamily: 'Geeza Pro'}}>Geeza Pro </Text>
        <Text style={{fontFamily: 'GeezaPro-Bold'}}>GeezaPro-Bold </Text>
        <Text style={{fontFamily: 'Georgia'}}>Georgia </Text>
        <Text style={{fontFamily: 'Georgia-Bold'}}>Georgia-Bold </Text>
        <Text style={{fontFamily: 'Georgia-BoldItalic'}}>Georgia-BoldItalic </Text>
        <Text style={{fontFamily: 'Georgia-Italic'}}>Georgia-Italic </Text>
        <Text style={{fontFamily: 'Gill Sans'}}>Gill Sans </Text>
        <Text style={{fontFamily: 'GillSans-Bold'}}>GillSans-Bold </Text>
        <Text style={{fontFamily: 'GillSans-BoldItalic'}}>GillSans-BoldItalic </Text>
        <Text style={{fontFamily: 'GillSans-Italic'}}>GillSans-Italic </Text>
        <Text style={{fontFamily: 'GillSans-Light'}}>GillSans-Light </Text>
        <Text style={{fontFamily: 'GillSans-LightItalic'}}>GillSans-LightItalic </Text>
        <Text style={{fontFamily: 'GillSans-SemiBold'}}>GillSans-SemiBold </Text>
        <Text style={{fontFamily: 'GillSans-SemiBoldItalic'}}>GillSans-SemiBoldItalic </Text>
        <Text style={{fontFamily: 'GillSans-UltraBold'}}>GillSans-UltraBold </Text>


        <Text style={{fontFamily: 'Heiti SC'}}>Heiti SC </Text>
        <Text style={{fontFamily: 'Heiti TC'}}>Heiti TC </Text>
        <Text style={{fontFamily: 'Helvetica'}}>Helvetica </Text>
        <Text style={{fontFamily: 'Helvetica Neue'}}>Helvetica Neue </Text>
        <Text style={{fontFamily: 'Helvetica-Bold'}}>Helvetica-Bold </Text>
        <Text style={{fontFamily: 'Helvetica-BoldOblique'}}>Helvetica-BoldOblique </Text>
        <Text style={{fontFamily: 'Helvetica-Light'}}>Helvetica-Light </Text>
        <Text style={{fontFamily: 'Helvetica-LightOblique'}}>Helvetica-LightOblique </Text>
        <Text style={{fontFamily: 'Helvetica-Oblique'}}>Helvetica-Oblique </Text>
        <Text style={{fontFamily: 'HelveticaNeue-Bold'}}>HelveticaNeue-Bold </Text>
        <Text style={{fontFamily: 'HelveticaNeue-BoldItalic'}}>HelveticaNeue-BoldItalic </Text>
        <Text style={{fontFamily: 'HelveticaNeue-CondensedBlack'}}>HelveticaNeue-CondensedBlack </Text>
        
      </ScrollView>
    );
  }
}
