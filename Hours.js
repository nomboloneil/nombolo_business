import React, { useState, useRef, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Switch, Animated, Button, TextInput, SafeAreaView } from 'react-native'
import platformText from './globalStyles'
import SwitchSelector from "react-native-switch-selector";
import DateTimePicker from '@react-native-community/datetimepicker';
import { set } from 'react-hook-form';
import UploadButton from './UploadButton';
const options = [
    { label: "AM", value: "AM", testID: "switch-one", accessibilityLabel: "switch-one" },
    { label: "PM", value: "PM", testID: "switch-two", accessibilityLabel: "switch-two" }
  ];
const days = [
    {
        id: 1,
        day: "monday",
        ab: "M"
    },
    {
        id: 2,
        day: "tuesday",
        ab: "Tu"
    },
    {
        id: 3,
        day: "wednesday",
        ab: "W"
    },
    {
        id: 4,
        day: "thursday",
        ab: "Th"
    },
    {
        id: 5,
        day: "friday",
        ab: "F"
    },
    {
        id: 6,
        day: "saturday",
        ab: "Sa"
    },
    {
        id: 7,
        day: "sunday",
        ab: "Su"
    }
]
const daysIds = days.map(day => day.id)
let chosenDays
let dayMap

export default function Hours() {
    const [selectedDays, setSelectedDays] = useState(new Set([]))
    const [select, setSelect] = useState([])

    return (
            <View style={{alignItems: "center", marginBottom: 150}}>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>Days</Text>
                <View style={{flexDirection: "row"}}>
                <DaysButton
                    selectedDays={selectedDays}
                    setSelectedDays={setSelectedDays}
                    dayMap={dayMap}
                    select={select}
                    setSelect={setSelect}
                />
                </View>
                <View>
                <HoursButtons
                    selectedDays={selectedDays}
                    dayMap={dayMap}
                    select={select}
                />
                </View>
            </View>
    )
}

function DaysButton (props) {
    useEffect(() => {
        console.log('select', props.select.flat())
    }, [props.select])
    const setDay = (dayIndex, selected) => {
        if (selected) {
            console.log('in here', selected, dayIndex)
            let stateCopy = props.selectedDays
            stateCopy.delete(dayIndex + 1)
            console.log("removed",stateCopy)
            props.setSelectedDays(new Set([...stateCopy]))
            return
        }
        
        dayMap = days.filter(day=> day.id === dayIndex + 1)
        props.setSelect((prevSelect) => [
            ...prevSelect,
            dayMap
        ])
        
        props.setSelectedDays(
            new Set([
                ...props.selectedDays,
                dayIndex + 1
            ])
        )
    }
    return (
            <>
            {days.map((day,index) => {
                let selected = props.selectedDays?.has(index + 1)
                return (
               <View style={{padding: 2}}>
                    <TouchableOpacity
                        style={selected ? styles.selectedButton : styles.unSelectedButton}
                        onPress={() =>setDay(index, selected)}>
                            <View style={selected ? styles.selectedVew : styles.unselectedView}>
                                <Text style={selected ? styles.selectedText : styles.unselectedText}>
                                {day.ab}
                                </Text>
                            </View>
                    </TouchableOpacity>
               </View>
                )
            })}
            </>
    )
}

function HoursButtons(props) {
    const mySet = new Set()

    const startHours = [
    {
        name: 'Opening',
        time: '7:00',
        meridiem: 'AM'
    },
    {
        name: 'Closing  ',
        time: '7:00',
        meridiem: 'PM'
    }
]
    const [hours, setHours] = useState(startHours)
    function addHours () {
        let newHours = hours.concat(startHours)
        return setHours(newHours)
    }
    function handleChange (input, index) {
        let copy = [...hours]
        copy[index].time = input
        setHours(copy)
        //console.log(copy)
    }
    function amPm (value, index) {
        //console.log('value', value)
       //console.log( hours[index].meridiem)
       //console.log('old hours',hours[index].meridiem)

       let copy = [...hours]

        copy[index].meridiem = value
        setHours(copy)
    //    let newHours = hours.splice(hours[index], 1)
    //    let updatedValue = newHours[0].meridiem = value
    // //    console.log('value',value)
    // //    console.log('newHours', updatedValue)
    // //    console.log('old hours again',hours)
    //     let copy = hours
        //copy[index].meridiem = value
       // console.log('copy',  copy[index].meridiem, value)

    }
    function compareArrays(a, b) {
        {
            let map = {};
a.forEach(i => map[i] = false);
b.forEach(i => map[i] === false && (map[i] = true));
let jsonArray = Object.keys(map).map(k => ({ name: k, matched: map[k] }));
return jsonArray
        }
    }

//     function sameArrays (object, array2) {
//        let array1 =  object.map(val => val.id)
//        let sorted1 = array1.sort()
//        let sorted2 = array2.sort()
//        console.log(sorted1, sorted2)
//       let res =  object.map(property => {
//            if (pr)
//        })
//     //    let common = []
//     //    let a_i = 0
//     //    let b_i = 0
//     //    while (a_i < array1.length && b_i < array2.length) {
//     //  if (sorted1[a_i] === sorted2[b_i]) {
//     //      common.push(sorted1[a_i]);
//     //      a_i++;
//     //      b_i++;
//     //  }
//     //  else if(sorted1[a_i] < sorted2[b_i]) {
//     //      a_i++;
//     //  }
//     //  else {
//     //      b_i++;
//     //  }
//     // }
//     // return common;
// }
    Array.prototype.diff = function(arr2) {
        var ret = [];
        this.sort();
        arr2.sort();
        for(var i = 0; i < this.length; i += 1) {
            if(arr2.indexOf(this[i]) > -1){
                ret.push(this[i]);
            }
        }
        return ret;
    };
    
    return (
        <>
        {/* <View style={{flexDirection: "row"}}> */}
        {/* <DatePicker/> */}
            {
                hours.map((times, index)=> {
                    //console.log(times.meridiem)
                    return (
                        <>
                            <View style={{flexDirection: "row"}}>
                                <View style={{marginRight: 30}}>
                                    <Text style={{padding: 10}}>{times.name}</Text>
                                </View>
                                <CustomTextInput
                                    time={times.time.concat(times.meridiem)}
                                    onChangeNumber={(input) =>handleChange(input, index)}
                                    number={times.number}

                                />
                                <View style={{paddingVertical: 10, paddingLeft: 20}}>
                                    <SwitchSelector
                                        buttonColor={'#5BA9AB'}
                                        backgroundColor={'gray'}
                                        borderRadius={10}
                                        style={{width: 100}}
                                        options={options}
                                        initial={0}
                                        onPress={value => amPm(value, index)}
                                        /> 
                                </View>
                            </View>
                          </>
                    )
                })
            }
            <Text onPress={addHours} style={{textAlign: 'right', color: "#F58050", fontSize: 15}}>+ Add More Hours</Text>
            {hours ? 
                <>
                <Text style={{fontWeight: 'bold'}}>Current Hours</Text>
                <View>
                <View style={{flexDirection: 'row'}}>
                {console.log('select as set', mySet.add(props.select.flat()))}
                     {
                         props.select.filter((v,i,a)=>a.findIndex(t=>(t.id === v.id))===i)
.map(selection => (
                        <Text style={{flexDirection: "row"}}>{`${selection.day}, ` ?? 'no selection'}</Text>
                    ))}
                </View>
               
                    {hours.map((times, index) => {
                        
                        let currentTime = times.time.concat(' ').concat(times.meridiem)
                       return (<View style={{flexDirection: 'row'}}>
                                { index % 2 ? <Text>-</Text> : null}

                                <Text style={{color: '#5BA9AB', padding: 10}}>{currentTime} </Text>
                                </View>
                       )
                
                    })}
                    </View>
                    
                    </>
            
            : null}

        {/* </View> */}
        </>
        
    )
}




  const CustomTextInput = (props) => {
    const [text, onChangeText] = React.useState("Useless Text");
  
    return (
      <SafeAreaView
        style={{width: 100 }}>
        
        <TextInput
          style={styles.input}
          onChangeText={props.onChangeNumber}
          value={props.number}
          placeholder={props.time}
          keyboardType="numeric"
        />
      </SafeAreaView>
    );
  };

//   const Switcher = () => {
//     const [isEnabled, setIsEnabled] = useState(false);
//     const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  
//     return (
//       <View style={styles.container}>
//         <Switch
//           trackColor={{ false: "#767577", true: "#81b0ff" }}
//           thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
//           ios_backgroundColor="#3e3e3e"
//           onValueChange={toggleSwitch}
//           value={isEnabled}
//         />
//       </View>
//     );
//   }

// export const DatePicker = () => {
//     const [date, setDate] = useState(new Date());
//     const [mode, setMode] = useState('time');
//     const [show, setShow] = useState(false);
  
//     const onChange = (event, selectedDate) => {
//       const currentDate = selectedDate || date;
//       setShow(Platform.OS === 'ios');
//       setDate(currentDate);
//     };
  
//     const showMode = (currentMode) => {
//       setShow(true);
//       setMode(currentMode);
//     };
  
//     const showDatepicker = () => {
//       showMode('date');
//     };
  
//     const showTimepicker = () => {
//       setDate(new Date())
//       showMode('time');
//     };
  
//     return (
//       <View style={{width: 100}}>
//         <View style={{flexDirection: 'row'}}>
//           <Text onPress={showTimepicker} title="Show date picker!" >Opening</Text>
//           {show && (
//           <DateTimePicker
//             testID="dateTimePicker"
//             value={date}
//             mode={mode}
//             is24Hour={true}
//             display="default"
//             onChange={onChange}
//           />
//         )}
//         </View>
//         <View>
//           <Text onPress={showTimepicker} title="Show time picker!" >Closing</Text>
//           {show && (
//           <DateTimePicker
//             testID="dateTimePicker"
//             value={date}
//             mode={mode}
//             is24Hour={true}
//             display="default"
//             onChange={onChange}
//           />
//         )}
//         </View>
        
//       </View>
//     );
//   };
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
      },
      input: {
        height: 40,
        borderColor: "lightgrey",
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center', 
      },
      selectedButton: {
        backgroundColor: "#5BA9AB", 
        width: 50, 
        height: 50, 
        borderRadius: 999, 
        alignItems: 'center', 
        padding: 15
      },
      unSelectedButton: {
        borderWidth: 1.5,
        backgroundColor: "transparent",
        color: "#20232a",
        width: 50, 
        height: 50, 
        borderColor: "black", 
        borderRadius: 999
        },
        selectedText: {...platformText, alignItems: 'center', textAlign: 'center', color: "white", fontSize: 16 },
        unselectedText: {...platformText, alignItems: 'center', textAlign: 'center', fontSize: 16},
        unselectedView: {
            justifyContent: 'center', //Centered vertically
            alignItems: 'center', // Centered horizontally
            flex:1,
         padding: 5
        },
        selectedVew: {padding: 1}
})


