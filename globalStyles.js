import { Platform } from 'react-native';



export const platformText = Platform.select({
    ios: {
      fontFamily: 'Helvetica',
    },
    android: {
      fontFamily: 'Roboto',
    }
  })
