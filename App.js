import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, SafeAreaView } from 'react-native';
import ProgressSlides from './ProgressSlides'

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
    <StatusBar style="auto" />
      <ProgressSlides/>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
